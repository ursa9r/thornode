/*
Thornode API

Thornode REST API.

API version: 1.121.0
Contact: devs@thorchain.org
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// KeysignResponse struct for KeysignResponse
type KeysignResponse struct {
	Keysign *KeysignInfo `json:"keysign,omitempty"`
	Signature *string `json:"signature,omitempty"`
}

// NewKeysignResponse instantiates a new KeysignResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewKeysignResponse() *KeysignResponse {
	this := KeysignResponse{}
	return &this
}

// NewKeysignResponseWithDefaults instantiates a new KeysignResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewKeysignResponseWithDefaults() *KeysignResponse {
	this := KeysignResponse{}
	return &this
}

// GetKeysign returns the Keysign field value if set, zero value otherwise.
func (o *KeysignResponse) GetKeysign() KeysignInfo {
	if o == nil || o.Keysign == nil {
		var ret KeysignInfo
		return ret
	}
	return *o.Keysign
}

// GetKeysignOk returns a tuple with the Keysign field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *KeysignResponse) GetKeysignOk() (*KeysignInfo, bool) {
	if o == nil || o.Keysign == nil {
		return nil, false
	}
	return o.Keysign, true
}

// HasKeysign returns a boolean if a field has been set.
func (o *KeysignResponse) HasKeysign() bool {
	if o != nil && o.Keysign != nil {
		return true
	}

	return false
}

// SetKeysign gets a reference to the given KeysignInfo and assigns it to the Keysign field.
func (o *KeysignResponse) SetKeysign(v KeysignInfo) {
	o.Keysign = &v
}

// GetSignature returns the Signature field value if set, zero value otherwise.
func (o *KeysignResponse) GetSignature() string {
	if o == nil || o.Signature == nil {
		var ret string
		return ret
	}
	return *o.Signature
}

// GetSignatureOk returns a tuple with the Signature field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *KeysignResponse) GetSignatureOk() (*string, bool) {
	if o == nil || o.Signature == nil {
		return nil, false
	}
	return o.Signature, true
}

// HasSignature returns a boolean if a field has been set.
func (o *KeysignResponse) HasSignature() bool {
	if o != nil && o.Signature != nil {
		return true
	}

	return false
}

// SetSignature gets a reference to the given string and assigns it to the Signature field.
func (o *KeysignResponse) SetSignature(v string) {
	o.Signature = &v
}

func (o KeysignResponse) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Keysign != nil {
		toSerialize["keysign"] = o.Keysign
	}
	if o.Signature != nil {
		toSerialize["signature"] = o.Signature
	}
	return json.Marshal(toSerialize)
}

type NullableKeysignResponse struct {
	value *KeysignResponse
	isSet bool
}

func (v NullableKeysignResponse) Get() *KeysignResponse {
	return v.value
}

func (v *NullableKeysignResponse) Set(val *KeysignResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableKeysignResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableKeysignResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableKeysignResponse(val *KeysignResponse) *NullableKeysignResponse {
	return &NullableKeysignResponse{value: val, isSet: true}
}

func (v NullableKeysignResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableKeysignResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


